package com.service;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.jws.WebService;

@Stateless
@WebService(
        portName = "CalculatorServicePort",
        serviceName = "CalculatorService",
        targetNamespace = "http://Calculator.com/wsdl",
        endpointInterface = "com.service.CalculatorWS")
public class Calculator implements CalculatorWS {
	
	@Override
	public String evaluate(String expression) {
		String[] expressionArr;
		Pattern pattern;
		Matcher matcher;
		String tempOperand = "";
		ArrayList<Double> operandNumber = new ArrayList<Double>();
		double[] operandNumberArr;
		int oprandsIndex = 0;
		String Result = "";
		ArrayList<String> operators = new ArrayList<String>();
		
		
		try {
			System.out.println(expression + " is sent by the SOAP");
			pattern = Pattern.compile("[a-zA-Z]+");
			matcher = pattern.matcher(expression);
			if(matcher.find()) {
				return "Invalid Expression : Alphanumeric Input is not Supported";
			}else {
				expressionArr = expression.split("");
				System.out.println("expressionArr ==> " + expressionArr.toString());
				for(int i=1;i<expressionArr.length;i++){
					System.out.println("expressionArr[i] == > " + expressionArr[i]);
					if(expressionArr[i] != null && expressionArr[i] != "" && expressionArr[i] != "null") {
						switch(expressionArr[i]){
							case "+":
							case "-":
							case "*":	
							case "/":
								if(tempOperand.length() > 0){
									try {
										operandNumber.add(new Double(Double.parseDouble(tempOperand)));
										tempOperand = "";
										if(Double.isNaN(operandNumber.get(oprandsIndex))){
											return "Invalid Expression : Operand is not Number";
										}
										else{
											System.out.println("\n Operator Added : " + expressionArr[i]);
											System.out.println("\n operand Added : " + operandNumber.get(oprandsIndex));
											operators.add(expressionArr[i]);
											oprandsIndex++;
										}
									}catch(Exception e) {
										return "Invalid Expression : Operand is not Number";
									}
								}else{
									return "Invalid Expression : Operator Operand Mismatch";
								}
								break;
			
							case " ":
								break;
			
							default :
								try {
									if(!expressionArr[i].equals(".")){
										if(Double.isNaN(Double.parseDouble(expressionArr[i])))
											return "1)Invalid Expression : Operator is not Supported!";
									}else {
										if(tempOperand.indexOf('.') > 0 && expressionArr[i].equals("."))
											return "Invalid Expression : Operand contains more than one '.' character!";
									}
									
								}catch(Exception e) {
										return "2) Invalid Expression : Operator is not Supported!";
								}
							tempOperand += expressionArr[i];
							System.out.println("tempOperand ==> " + tempOperand);
							System.out.println(Double.parseDouble(tempOperand));
							if(tempOperand.length() > 0 && i==(expressionArr.length-1)){
								operandNumber.add(Double.parseDouble(tempOperand));
								tempOperand = "";
								try {
								if(Double.isNaN(operandNumber.get(oprandsIndex))){
									return "Invalid Expression : Operand is not Number";
								}
								else
									System.out.println("\n operand Added : " + operandNumber.get(oprandsIndex));
								}catch(Exception e) {
									return "Invalid Expression : Operand is not Number";
								}
							}
						}	
					}
				}
				
				System.out.println("\nTotal number of Operands in the Expression are : " + (operandNumber.size()));
				System.out.println("\nThe Total Number of Operators in the Expression are :" + (operators.size()));

				if((operandNumber.size()-1) != (operators.size())){
					return "Invalid Expression : Operator Operand Mismatch";	
				}else{
					operandNumberArr = new double[operators.size()];
					for(int i=0;i<operators.size();i++){

						switch(operators.get(i)){

						case "+":
							operandNumber.set((i+1), operandNumber.get(i) + operandNumber.get(i+1));
							break;

						case "-":
							operandNumber.set((i+1), operandNumber.get(i) - operandNumber.get(i+1));
							break;

						case "*":
							operandNumber.set((i+1), operandNumber.get(i) * operandNumber.get(i+1));
							break;

						case "/":
							operandNumber.set((i+1), operandNumber.get(i) / operandNumber.get(i+1));
							break;

						default:
							break;
						}
					}
					
					Result = String.valueOf(operandNumber.get(oprandsIndex));
					
					if(Result.equals("Infinity"))
						Result = "Divide by '0' is Invalid Operation";
				}
			}
		}catch(Exception e) {
			Result = "Invalid Expression!!!";
		}
		return Result;
	}
}
