package com.service;

import javax.jws.WebService;

@WebService(targetNamespace = "http://calculatorService.com/wsdl")
public interface CalculatorWS {
	
	public String evaluate(String expression);

}
